from flask import Flask
from pymongo import MongoClients
from config import Config

app = Flask(__name__)
app.config.from_object(Config)
client = MongoClient('localhost:27017')
db = client.myFirstMB

#login = LoginManager(app)

from app import routes, models
